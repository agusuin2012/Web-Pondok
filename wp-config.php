<?php
/**
 * Konfigurasi dasar WordPress.
 *
 * Berkas ini berisi konfigurasi-konfigurasi berikut: Pengaturan MySQL, Awalan Tabel,
 * Kunci Rahasia, Bahasa WordPress, dan ABSPATH. Anda dapat menemukan informasi lebih
 * lanjut dengan mengunjungi Halaman Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Menyunting wp-config.php}. Anda dapat memperoleh pengaturan MySQL dari web host Anda.
 *
 * Berkas ini digunakan oleh skrip penciptaan wp-config.php selama proses instalasi.
 * Anda tidak perlu menggunakan situs web, Anda dapat langsung menyalin berkas ini ke
 * "wp-config.php" dan mengisi nilai-nilainya.
 *
 * @package WordPress
 */

// ** Pengaturan MySQL - Anda dapat memperoleh informasi ini dari web host Anda ** //
/** Nama basis data untuk WordPress */
define( 'DB_NAME', 'web-pondok' );

/** Nama pengguna basis data MySQL */
define( 'DB_USER', 'root' );

/** Kata sandi basis data MySQL */
define( 'DB_PASSWORD', '' );

/** Nama host MySQL */
define( 'DB_HOST', 'localhost' );

/** Set Karakter Basis Data yang digunakan untuk menciptakan tabel basis data. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Jenis Collate Basis Data. Jangan ubah ini jika ragu. */
define('DB_COLLATE', '');

/**#@+
 * Kunci Otentifikasi Unik dan Garam.
 *
 * Ubah baris berikut menjadi frase unik!
 * Anda dapat menciptakan frase-frase ini menggunakan {@link https://api.wordpress.org/secret-key/1.1/salt/ Layanan kunci-rahasia WordPress.org}
 * Anda dapat mengubah baris-baris berikut kapanpun untuk mencabut validasi seluruh cookies. Hal ini akan memaksa seluruh pengguna untuk masuk log ulang.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!?uQ,H-bl:L#QHB*!u:ETlwx%ipJuc$^24O.F]K<ux?Fj$+Ek<NLHTX>M<y]nrFD' );
define( 'SECURE_AUTH_KEY',  'nSWH[r!Y#9<h,rz;gw30Y%]shmD2FLG>l5r^UfNb7Uq#w(1^@=_~ltU:+p`$&OW^' );
define( 'LOGGED_IN_KEY',    '1vpn1.kHHfdE=k5-_[^:Yb]Jn6Tw})H2qr3&L:j.3jyhFPQdm+Y4ls!IM0jg^P_C' );
define( 'NONCE_KEY',        '%0)SN-JZ<=(D,MEN*LExzC;.yG|^7~?D}5GJ~M,rp9k(*kKy19to~1~2%D3ldb7$' );
define( 'AUTH_SALT',        '.#,&iepP)A@g/f@ IR+F2PUp%LcE;BbVKf|.:5dz$g/}V$,#GZ;70l{##/W,^=t0' );
define( 'SECURE_AUTH_SALT', 'fhg.4_KG1@+lCP=qpvymnD=6D2;Tb%{1,@-6I ]1VhP$:QLs%SOH3+D#gh$rfRhJ' );
define( 'LOGGED_IN_SALT',   'BL}/{nJLZ(=a45H*;dwEbj~:m&^ v^l|})J{^HT2<xTCiBWDfL^?{vTu1/>E+*8@' );
define( 'NONCE_SALT',       '<:~]l`v:/xka5sccINpU IIc9m9votTq0=s[s =HZh{aXij~{T6Ev<9/k%3btqf=' );

/**#@-*/

/**
 * Awalan Tabel Basis Data WordPress.
 *
 * Anda dapat memiliki beberapa instalasi di dalam satu basis data jika Anda memberikan awalan unik
 * kepada masing-masing tabel. Harap hanya masukkan angka, huruf, dan garis bawah!
 */
$table_prefix = 'wp_';

/**
 * Untuk pengembang: Moda pengawakutuan WordPress.
 *
 * Ubah ini menjadi "true" untuk mengaktifkan tampilan peringatan selama pengembangan.
 * Sangat disarankan agar pengembang plugin dan tema menggunakan WP_DEBUG
 * di lingkungan pengembangan mereka.
 */
define('WP_DEBUG', false);

/* Cukup, berhenti menyunting! Selamat ngeblog. */

/** Lokasi absolut direktori WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Menentukan variabel-variabel WordPress berkas-berkas yang disertakan. */
require_once(ABSPATH . 'wp-settings.php');
